// Replace the projectName and projectKey with your SonarQube values
def projectName = 'template-microservice'
def projectKey = 'io.digital.supercharger:template-microservice'
pipeline {
    agent {
        label 'java'
    }

    stages {
        stage ('Compile') {
            steps {
               sh 'printenv'
               configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                   sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS compile'
               }
            }
        }

		stage ('Code analysis') {
			steps {
			   configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
			        sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS checkstyle:check pmd:check spotbugs:check'
			   }
			}
		}

		stage ('Unit Tests') {
			steps {
			    configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
				    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS test'
				}
 				jacoco(
                      execPattern: 'target/*.exec',
                      classPattern: 'target/classes',
                      sourcePattern: 'src/main/java',
                      exclusionPattern: 'src/test*'
                )
			}
		}

		stage ('Integration Tests') {
            steps {
                // Skip everything apart from the integration tests
                configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS verify -Dskip.surefire.tests -Dspotbugs.skip=true -Dpmd.skip=true -Dcheckstyle.skip=true -Ddockerfile.skip'
                }
            }
        }

		stage("build & SonarQube analysis") {
            steps {
              withSonarQubeEnv('sonar') {
                  configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS sonar:sonar'
                  }
              }
            }
          }


		stage('SQ Quality Gate') {
		  steps {
			timeout(time: 5, unit: 'MINUTES') {
			    waitForQualityGate abortPipeline: true
			}
		  }
		}

		stage ('Package') {
		    steps {
			    // package will tag with git-hash and also latest
			    configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
				    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS package -DskipTests=true -Dskip.surefire.tests'
				}
			}
		}
	}
	post {
        always {
            junit 'target/surefire-reports/**/*.xml'

            deleteDir() /* clean up our workspace */
        }
     }
}
