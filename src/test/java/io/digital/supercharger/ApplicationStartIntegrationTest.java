package io.digital.supercharger;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@TestPropertySource("classpath:application.properties")
class ApplicationStartIntegrationTest {

  @Test
  void testMain() {
    MicroserviceApplication.main(new String[] {});
    assertTrue(true, "If I run, Means Application context is Able to load!");
  }
}
