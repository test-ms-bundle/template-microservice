package io.digital.supercharger.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.digital.supercharger.dto.DemoData;
import io.digital.supercharger.model.Demo;
import io.digital.supercharger.repository.DemoRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class DefaultDemoServiceTest {
  @Mock private DemoRepository demoRepository;
  private DefaultDemoService defaultDemoService;

  @BeforeEach
  void setup() {
    defaultDemoService = new DefaultDemoService(demoRepository);
  }

  @Test
  void findAll() {
    List<Demo> itemsInDb = new ArrayList<>(2);
    itemsInDb.add(new Demo(1L, "test 1"));
    itemsInDb.add(new Demo(2L, "test 2"));

    when(demoRepository.findAll()).thenReturn(itemsInDb);
    List<DemoData> demoDataList = defaultDemoService.findAll();
    verify(demoRepository, times(1)).findAll();

    assertEquals(2, demoDataList.size());
  }

  @Test
  void save() {
    when(demoRepository.save(any())).thenReturn(new Demo(1L, "test 2"));

    defaultDemoService.save(new DemoData(1L, "test 2"));
    ArgumentCaptor<Demo> argument = ArgumentCaptor.forClass(Demo.class);
    verify(demoRepository).save(argument.capture());
    assertEquals(Long.valueOf(1L), argument.getValue().getId());
    assertEquals("test 2", argument.getValue().getName());
  }

  @Test
  void findById() {
    long id = 1L;
    when(demoRepository.findById(id)).thenReturn(Optional.of(new Demo(id, "test 1")));
    DemoData demoData = defaultDemoService.findById(id);
    verify(demoRepository, times(1)).findById(id);

    assertEquals(Long.valueOf(id), demoData.getId());
    assertEquals("test 1", demoData.getName());
  }

  @Test
  void findByIdError() {
    long id = 1L;
    when(demoRepository.findById(id)).thenReturn(Optional.empty());
    Exception exception =
        assertThrows(EntityNotFoundException.class, () -> defaultDemoService.findById(id));

    assertEquals("Demo not found.", exception.getMessage());

    verify(demoRepository, times(1)).findById(id);
  }
}
