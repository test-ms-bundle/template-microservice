package io.digital.supercharger.repository;

import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertFalse;

import io.digital.supercharger.model.Demo;
import java.util.Optional;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ActiveProfiles("test") // Active profile test is needed to exclude swagger from loading
@Tag("IntegrationTest")
class DemoRepositoryTest {

  @Autowired private TestEntityManager entityManager;

  @Autowired private DemoRepository demoRepository;

  @Test
  void itShouldSaveDemoAndFind() {
    final long id = 1L;
    Demo demo = new Demo();
    demo.setName("test");
    entityManager.persistAndFlush(demo);

    assertTrue(demoRepository.findById(id).isPresent());
    assertThat(demoRepository.findById(id).get()).isEqualTo(demo);
  }

  @Test
  void itShouldNotFoundDemo() {
    Optional<Demo> not_found = demoRepository.findById(2L);
    assertFalse(not_found.isPresent());
  }
}
