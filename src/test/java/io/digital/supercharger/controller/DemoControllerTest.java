package io.digital.supercharger.controller;

import static io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor.REQUEST_ATTRIBUTE_CUSTOMER_PROFILE;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import io.digital.supercharger.TestHelper;
import io.digital.supercharger.common.dto.CustomerProfileDto;
import io.digital.supercharger.common.exception.PermissionException;
import io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor;
import io.digital.supercharger.common.util.JsonUtil;
import io.digital.supercharger.dto.DemoData;
import io.digital.supercharger.exception.RestExceptionHandler;
import io.digital.supercharger.service.DemoService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
class DemoControllerTest {

  @Mock AuthenticationInterceptor interceptor;
  private MockMvc mockMvc;
  @Mock private DemoService demoService;

  @BeforeEach
  void setUp() throws Exception {
    when(interceptor.preHandle(
            any(HttpServletRequest.class), any(HttpServletResponse.class), any()))
        .thenReturn(true);

    this.mockMvc =
        standaloneSetup(new DemoController(demoService))
            .setControllerAdvice(new RestExceptionHandler())
            .addInterceptors(interceptor)
            .build();
  }

  @Test
  void getAll() throws Exception {
    List<DemoData> itemDtoList = new ArrayList<>(2);
    itemDtoList.add(new DemoData());
    itemDtoList.add(new DemoData());
    when(demoService.findAll()).thenReturn(itemDtoList);
    this.mockMvc
        .perform(MockMvcRequestBuilders.get(TestHelper.DEMO_URL))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(2)));
  }

  @Test
  void saveDemo() throws Exception {
    DemoData demo = new DemoData(1L, "test");
    when(demoService.save(any(DemoData.class))).thenReturn(demo);
    this.mockMvc
        .perform(
            post(TestHelper.DEMO_URL)
                .content(JsonUtil.toJson(demo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    verify(demoService, times(1)).save(any(DemoData.class));
  }

  @Test
  void getDemoById() throws Exception {
    when(demoService.findById(1L)).thenReturn(new DemoData(1L, "test"));
    CustomerProfileDto customerProfileDto = CustomerProfileDto.builder().username("demo").build();
    this.mockMvc
        .perform(
            get(TestHelper.DEMO_URL + "/" + 1)
                .requestAttr(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE, customerProfileDto))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
  }

  @Test
  void handleEntityNotFoundErrorResponse() throws Exception {
    when(demoService.findById(1L)).thenThrow(new EntityNotFoundException());
    this.mockMvc.perform(get(TestHelper.DEMO_URL + "/" + 1)).andExpect(status().is4xxClientError());
  }

  @Test
  void handleAuthenticationErrorResponse() throws Exception {

    when(interceptor.preHandle(
            any(HttpServletRequest.class), any(HttpServletResponse.class), any()))
        .thenThrow(new PermissionException("Unauthorized"));
    this.mockMvc.perform(get(TestHelper.DEMO_URL + "/" + 1)).andExpect(status().isForbidden());
  }

  @Test
  void handleRuntimeExceptionErrorResponse() throws Exception {
    when(demoService.findById(1L)).thenThrow(new IllegalArgumentException());
    when(interceptor.preHandle(
            any(HttpServletRequest.class), any(HttpServletResponse.class), any()))
        .thenReturn(true);
    this.mockMvc.perform(get(TestHelper.DEMO_URL + "/" + 1)).andExpect(status().is4xxClientError());
  }
}
