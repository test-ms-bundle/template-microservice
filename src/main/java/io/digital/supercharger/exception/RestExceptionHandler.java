package io.digital.supercharger.exception;

import io.digital.supercharger.common.exception.CustomRestExceptionHandler;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
@Log4j2
public class RestExceptionHandler extends CustomRestExceptionHandler {}
