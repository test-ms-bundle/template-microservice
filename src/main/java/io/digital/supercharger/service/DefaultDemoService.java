package io.digital.supercharger.service;

import io.digital.supercharger.dto.DemoData;
import io.digital.supercharger.model.Demo;
import io.digital.supercharger.repository.DemoRepository;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultDemoService implements DemoService {

  private final DemoRepository demoRepository;

  @Autowired
  public DefaultDemoService(DemoRepository demoRepository) {
    this.demoRepository = demoRepository;
  }

  /** @{inheritDoc}. */
  @Override
  public List<DemoData> findAll() {
    return demoRepository.findAll().stream()
        .map(demo -> new DemoData(demo.getId(), demo.getName()))
        .collect(Collectors.toList());
  }

  /** @{inheritDoc}. */
  @Override
  public DemoData save(DemoData demoData) {
    Demo demo = demoRepository.save(new Demo(demoData.getId(), demoData.getName()));
    return new DemoData(demo.getId(), demo.getName());
  }

  /** @{inheritDoc}. */
  @Override
  public DemoData findById(Long demoId) {
    return demoRepository
        .findById(demoId)
        .map(demo -> new DemoData(demo.getId(), demo.getName()))
        .orElseThrow(() -> new EntityNotFoundException("Demo not found."));
  }
}
