# Template microservice java

Spring template application is a Java SpringBoot microservice that exposes some REST APIs. The microservice follow the domain-driven design pattern e.g. it has controllers, model, services and repositories. The microservice is part of a small group of microservices used as a playground to provide a boilerplate for:

- Creating a Java SpringBoot microservice with REST exposed APIs that are documented using [Swagger](https://swagger.io/)
- Was initially created using the [Spring Initializr](https://start.spring.io/) 
- Logging and error handling
- Data persistence using Spring Data JPA
- Monitoring and health checks [Health Url](http://localhost:8080/actuator)
- Configurations management (TODO)
- Authentication (TODO)    

# Requirements:

- Apache Maven 3.3
- Java 11

# Application Architecture & Technology Stack

This repository implements the following quality gates:

- Static code checks: by running [Checkstyle](http://checkstyle.sourceforge.net/), [PMD](https://pmd.github.io/) and [SpotBugs](https://spotbugs.github.io/) to check the code for any style or quality issues. Checkstyle is based on the [Google Java Standards](https://google.github.io/styleguide/javaguide.html) 
- Static code analysis using [SonarQube](https://www.sonarqube.org/)
- Unit testing: using [JUnit](https://junit.org/junit4/), mocking using [JMockit](http://jmockit.github.io/) and [Mockito](https://site.mockito.org/) (using both only as an example of each)
- Integration testing: using [SpringBootTest](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html). A good example can be found [here](https://dzone.com/articles/integration-testing-in-spring-boot-1)
- Code coverage: generating code coverage reports using the [JaCoCo](https://www.jacoco.org/jacoco/)
- Docker image build: using the Spotify [docker maven plugin](https://github.com/spotify/docker-maven-plugin)
- Pushing to image registry: pushing the docker image to the [Docker hub](https://hub.docker.com/)

These steps can be run manually or using a Continuous Integration tool such as [Jenkins](https://jenkins.io/). More information about setting up Jenkins and SonarQube are explained in the [Build via Jenkins (Jenkinsfile)](build-via-jenkins-jenkinsfile) section below

## Major Libraries / Tools

| Category                           | Library/Tool      | Link                                                          |
|---------------------------------   |----------------   |------------------------------------------------------------   |
| Microservice Framework             | Spring Boot       | https://spring.io/projects/spring-boot                        |
| Dependencies Management            | Spring            | https://docs.spring.io                                        |
| Object Relational Mapping (ORM)    | Spring Data JPA   | https://spring.io/projects/spring-data-jpa                    |
| Boilerplate Code Generation        | Lombok (see below) | https://projectlombok.org/                                   |
| Automate Build & Release           | Maven             | https://docs.fastlane.tools/actions/xcodebuild/               |
| Unit Testing                       | JUnit             | https://junit.org/junit4/                                     |
| Mocking (examples of two libraries | Mockito & JMockit | http://jmockit.github.io/ and https://site.mockito.org/       |
| Code Coverage                      | JaCoCo            | https://www.jacoco.org/jacoco/                                |
| Static Code Style Check (Lint)     | Checkstyle        | http://checkstyle.sourceforge.net/                            |
| Static Code Analysis               | PMD and SpotBugs  | https://pmd.github.io/ and https://spotbugs.github.io/        |
| Integration Testing                | SpringBootTest    | https://dzone.com/articles/integration-testing-in-spring-boot-1    |
| Docker Image Storage               | Docker Hub        | https://hub.docker.com/                                       |
| Continous Integration              | Jenkins           | https://jenkins.io/                                           |
| Static Code Analysis Integration   | SonarQube         | https://www.sonarqube.org/                                    |

## Below is a list of Maven plugins used:

* **spring-boot-maven-plugin**
* **maven-jar-plugin**
* **maven-dependency-plugin**
* **git-commit-id-plugin**: extract part of the Git commit SHA
* **dockerfile-maven-plugin**: package the jar as a Docker container
* **maven-checkstyle-plugin**: run Checkstyle using the [./checkstyle.xml](./checkstyle.xml) in the root of the project. This is based on the [Google Java Style](https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml)
* **spotbugs-maven-plugin**: run Spotbugs static analysis
* **maven-pmd-plugin**: run PMD static analysis
* **maven-surefire-plugin**: run the unit tests
* **maven-failsafe-plugin**: run the integration tests
* **jacoco-maven-plugin**: run the code coverage
* **awaitility**: to handle the thread wait during testing async methods

## Boilerplate Code Generation Using Lombok

This project used [Lombok](https://projectlombok.org/) to automatically generate boilerplate code such as getter, setters, constructors, etc. It even includes a builder patter. Lombok is compile time only and is setup in Maven as `provided`. To install it for Eclipse follow the instructions [here](https://projectlombok.org/setup/eclipse).

## POJO Mapping Using Mapstruct

This project provides an example of [MapStruct](http://mapstruct.org) to easily copy data between POJOs. MapStruct is a code generator that greatly simplifies the implementation of mappings between Java bean types based on a convention over configuration approach. You must follow the instructions [here](http://mapstruct.org/documentation/ide-support/) to add it to your IDE.


## EditorConfig

To ensure editor settings are the same for all developers, the project contains a `.editorconfig` file to set editor config such as **indent**, **end of line character**. It's recommended to install the EditorConfig plugin for Eclipse [https://marketplace.eclipse.org/content/editorconfig-eclipse](https://marketplace.eclipse.org/content/editorconfig-eclipse)


# Getting Started


Clone and build the project

```bash
    mvn clean install
```

## Swagger Docs

**The Swagger Docs can be accessed on the following URLs:**

[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

[http://localhost:8080/v2/api-docs](http://localhost:8080/v2/api-docs)

# CI-CD - Build via Jenkins (Jenkinsfile)

This repo contains a [Jenkinsfile](./Jenkinsfile) [https://jenkins.io/doc/book/pipeline/jenkinsfile/](https://jenkins.io/doc/book/pipeline/jenkinsfile/), which is used to define a Jenkins **declarative pipeline** for CI-CD to build the code, run the quality gates, code coverage, static analysis and deploy to Fabric. Here is an example structure of the Jenkinsfile declarative pipeline:

```
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building..'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
}
```
